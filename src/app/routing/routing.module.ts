import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from '../pages/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { HeroService } from '../services/hero.service';
import { SentinelsHeroListComponent } from '../components/sentinels-hero-list/sentinels-hero-list.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { VillainslistComponent } from '../components/villainslist/villainslist.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/heroes',
        pathMatch: 'prefix'
    },
    {
        path: 'heroes',
        component: SentinelsHeroListComponent
    },
    {
        path: 'villains',
        component: VillainslistComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    exports: [ RouterModule ],
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules,
        }),
    ],
    providers: [
        HeroService,
    ]
})
export class RoutingModule {
}

export const routingComponents = [
    HomeComponent,
    SentinelsHeroListComponent,
    PageNotFoundComponent,
];
