export class Profile {
    constructor(
        public id: string,
        public name: string,
        public primary_class: string,
        public secondary_class: string,
        public image: string,
        public complexity: number,
        public box: string
    ) {
    }
}

export interface IProfile {
    id: string;
    name: string;
    primary_class: string;
    secondary_class: string;
    image: string;
    complexity: number;
    box: string;
}

export interface IProfileGroup {
    hero: IProfile;
    villain: IProfile;
}
