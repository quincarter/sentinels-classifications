export enum SearchKeys {
    Backspace = 8,
    Tab = 9,
    Enter = 13,
    Shift = 16,
    Escape = 27,
    ArrowLeft = 37,
    ArrowRight = 39,
    ArrowUp = 38,
    ArrowDown = 40,
    MacCommandLeft = 91,
    MacCommandRight = 93,
    MacCommandFirefox = 224,
}

export enum SearchKeyNames {
    ArrowUp = 'ArrowUp',
    ArrowDown = 'ArrowDown',
    ArrowLeft = 'ArrowLeft',
    ArrowRight = 'ArrowRight',
    Enter = 'Enter',
    Escape = 'Escape',
}