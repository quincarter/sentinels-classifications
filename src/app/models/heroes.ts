export class Hero {
    constructor(public id: string,
                public name: string,
                public primary_class: string,
                public secondary_class: string,
                public image: string,
                public box: string,
                public complexity: number) {
    }
}

export interface IHeroResponse {
    data: Hero[];
}

export interface IHero {
    data: HeroInterface;
}

export interface HeroInterface {
    id: string;
    name: string;
    primary_class: string;
    secondary_class: string;
    image: string;
    box: string;
    complexity: number;
}

export declare type HeroType =
    'Damage'
    | 'Tank'
    | 'Support'
    | 'Damage and Tanking'
    | 'Jack of all Trades'
    | 'Utility'
    | 'Buffs';
