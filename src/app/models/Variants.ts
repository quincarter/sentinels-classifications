export class Variant {
    constructor(
        public card_image: string,
        public hp: number,
        public name: string,
        public backstory: string,
        public powers: [{
            name: string;
            description: string;
        }]
    ) {
    }
}

export class Powers {
    constructor(
        public name: string,
        public description: string
    ) {}
}

export interface IVariantGroup {
    variants: IVariant;
}

export interface IVariant {
    card_image: string;
    hp: number;
    name: string;
    backstory: string;
    powers: [
        IPower
        ];
}

export interface IPower {
    description: string;
    name: string;
}
