import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModulesModule } from './materialImports/angularMaterialModules.module';
import { SentinelsModule } from './components/sentinels.module';
import { RoutingModule } from './routing/routing.module';
import { PagesModule } from './pages/pages.module';
import { MAT_LABEL_GLOBAL_OPTIONS } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeroService } from './services/hero.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        AngularMaterialModulesModule,
        BrowserAnimationsModule,
        BrowserModule,
        SentinelsModule,
        PagesModule,
        RoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireStorageModule,
        AngularFirestoreModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [
        HeroService,
        {
            provide: MAT_LABEL_GLOBAL_OPTIONS,
            useValue: {
                float: 'never'
            }
        }
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
}
