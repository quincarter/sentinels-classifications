import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { IProfileGroup } from '../../models/profile.model';
import { Observable, Subscription } from 'rxjs/index';
import { HeroService } from '../../services/hero.service';
import { IVariant } from '../../models/Variants';

@Component({
    selector: 'app-sentinels-profile',
    templateUrl: './sentinels-profile.component.html',
    styleUrls: [ './sentinels-profile.component.scss' ]
})
export class SentinelsProfileComponent implements OnDestroy, OnInit {

    public variantObservable: Observable<IVariant>;

    private variantSubscription: Subscription;

    @Input() profileInfo: IProfileGroup;

    constructor(
        private heroService: HeroService,
    ) {
    }

    ngOnInit() {
        // subscribing to variants first so that when the next line happens, it will be seamless
        this.getVariantSubscription();
        this.heroService.getVariants(this.profileInfo.hero.id);
    }

    ngOnDestroy(): void {
        this.variantSubscription.unsubscribe();
    }

    getVariantSubscription(): void {
        this.variantSubscription = this.heroService.variants$.subscribe(variants => {
            this.variantObservable = variants;
        });
    }

}
