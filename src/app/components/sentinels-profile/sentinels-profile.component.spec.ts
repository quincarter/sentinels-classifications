import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsProfileComponent } from './sentinels-profile.component';

describe('SentinelsProfileComponent', () => {
  let component: SentinelsProfileComponent;
  let fixture: ComponentFixture<SentinelsProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
