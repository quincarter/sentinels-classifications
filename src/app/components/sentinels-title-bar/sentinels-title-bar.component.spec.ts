import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsTitleBarComponent } from './sentinels-title-bar.component';

describe('SentinelsTitleBarComponent', () => {
  let component: SentinelsTitleBarComponent;
  let fixture: ComponentFixture<SentinelsTitleBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsTitleBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsTitleBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
