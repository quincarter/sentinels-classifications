import { Component, OnDestroy, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { Observable, Subscription } from 'rxjs/index';

@Component({
  selector: 'app-sentinels-title-bar',
  templateUrl: './sentinels-title-bar.component.html',
  styleUrls: [ './sentinels-title-bar.component.scss']
})
export class SentinelsTitleBarComponent implements OnDestroy, OnInit {

    public title = 'Heroes List';
    public searchToggle: Observable<boolean>;
    public searchToggleValue: boolean;
    private searchSubscription: Subscription;

    constructor(
        private searchService: SearchService
    ) {
    }

  ngOnInit() {
        this.getSearchSubscription();
  }

  ngOnDestroy() {
        this.searchService.toggleSearch(false);
        this.searchSubscription.unsubscribe();
  }

  toggleSearch(): void {
      this.searchToggle = this.searchService.toggleSearch(!this.searchToggleValue);
  }

  getSearchSubscription(): void {
        this.searchSubscription = this.searchService.getSearchToggleStatus().subscribe(data => {
            this.searchToggleValue = data.valueOf();
        });
  }

}
