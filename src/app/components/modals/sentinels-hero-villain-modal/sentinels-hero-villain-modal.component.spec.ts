import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsHeroVillainModalComponent } from './sentinels-hero-villain-modal.component';

describe('SentinelsHeroVillainModalComponent', () => {
  let component: SentinelsHeroVillainModalComponent;
  let fixture: ComponentFixture<SentinelsHeroVillainModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsHeroVillainModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsHeroVillainModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
