import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SentinelsSearchComponent } from '../../sentinels-search/sentinels-search.component';
import { IProfile } from '../../../models/profile.model';

@Component({
    selector: 'app-sentinels-hero-villain-modal',
    templateUrl: './sentinels-hero-villain-modal.component.html',
    styleUrls: [ './sentinels-hero-villain-modal.component.scss' ]
})

export class SentinelsHeroVillainModalComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<SentinelsSearchComponent>,
        @Inject(MAT_DIALOG_DATA) public profile: DialogData) {

    }

    ngOnInit() {
    }

}

export interface DialogData {
    hero: IProfile;
    villain: IProfile;
}
