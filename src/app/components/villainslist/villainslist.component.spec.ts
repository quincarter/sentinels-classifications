import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VillainslistComponent } from './villainslist.component';

describe('VillainslistComponent', () => {
  let component: VillainslistComponent;
  let fixture: ComponentFixture<VillainslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VillainslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VillainslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
