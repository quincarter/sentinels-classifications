import {
    Component,
    ElementRef,
    HostListener,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs/index';
import { isEnterKey, SearchService } from '../../services/search.service';
import { HeroService } from '../../services/hero.service';
import { Hero, IHero } from '../../models/heroes';
import { MatDialog } from '@angular/material';
import { SentinelsHeroVillainModalComponent } from '../modals/sentinels-hero-villain-modal/sentinels-hero-villain-modal.component';
import { MAIN } from '../main';

@Component({
    selector: 'app-sentinels-search',
    templateUrl: './sentinels-search.component.html',
    styleUrls: [ './sentinels-search.component.scss' ]
})
export class SentinelsSearchComponent implements OnDestroy, OnInit {

    public searchToggleValue: boolean;
    public searchResultsList: any;
    public displayResults: boolean;
    public noResults: boolean;
    public isMobile = false;
    public isDesktop = false;
    public resultBoxHeight = null;

    private searchSubscription: Subscription;
    private heroSubscription: Subscription;
    private keyupSubject: Subject<KeyboardEvent> = new Subject<KeyboardEvent>();

    public options: FormGroup;

    constructor(fb: FormBuilder,
                public dialog: MatDialog,
                private searchService: SearchService,
                private heroService: HeroService) {
        this.options = fb.group({
            floatLabel: 'never',
        });
    }

    @ViewChild('searchInput') searchInputFieldVariable: ElementRef;
    @ViewChild('searchForm') searchForm: ElementRef;

    @HostListener('keyup', ['$event'])
    onKeyup(event: KeyboardEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.keyupSubject.next(event);
        this.searchResults();
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        if (isEnterKey(event)) {
            event.preventDefault();
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth >= MAIN.MQ_BREAKPOINTS.TABLET) {
            this.isMobileResolution(false);
            this.isDesktopResolution(true);
        } else {
            this.isDesktopResolution(false);
            this.isMobileResolution(true);
        }

        if (this.isDesktop) {
            if (event.target.innerHeight < 770) {
                this.resultBoxHeight = event.target.innerHeight - 135;
            } else {
                this.resultBoxHeight = null;
            }
        } else if (this.isMobile) {
            this.resultBoxHeight = event.target.innerHeight - 64; // 56px is the height of the toolbar
        }

        if (event.target.innerHeight > 770) {
            this.resultBoxHeight = null;
        }
    }

    ngOnInit(): void {
        this.displayResults = false;

        if (window.innerWidth >= MAIN.MQ_BREAKPOINTS.TABLET) {
            this.isMobileResolution(false);
            this.isDesktopResolution(true);
        } else {
            this.isDesktopResolution(false);
            this.isMobileResolution(true);
        }

        if (this.isDesktop) {
            if (window.innerHeight < 770) {
                this.resultBoxHeight = window.innerHeight - 135;
            } else {
                this.resultBoxHeight = null;
            }
        } else if (this.isMobile) {
            this.resultBoxHeight = window.innerHeight - 64; // 56px is the height of the toolbar
        }

        if (window.innerHeight > 770 && !this.isMobile) {
            this.resultBoxHeight = null;
        }

        this.getHeroSubscription();
        this.searchResultsList = this.heroService.getSearchList();
        this.getSearchSubscription();
    }

    ngOnDestroy() {
        this.searchService.toggleSearch(false);
        this.searchSubscription.unsubscribe();
        this.heroSubscription.unsubscribe();
    }

    toggleSearch(): void {
        this.searchService.toggleSearch(!this.searchToggleValue);
    }

    getSearchSubscription(): void {
        this.searchSubscription = this.searchService.getSearchToggleStatus().subscribe(data => {
            this.searchToggleValue = data;
        });
    }

    getHeroSubscription(): void {
        this.heroSubscription = this.heroService.heroes$.subscribe(heroes => {
            // this.searchResultsList = heroes;
        });
    }

    searchResults(): void {
        const searchInput = this.searchInputFieldVariable.nativeElement.value.toLowerCase();
        if (searchInput) {
            this.searchResultsList = this.heroService.search(searchInput);
        }

        this.displaySearchResults(this.searchResultsList);
    }

    isMobileResolution(check: boolean): void {
        this.isMobile = check;
    }

    isDesktopResolution(check: boolean): void {
        this.isDesktop = check;
    }

    openHeroModal(hero: IHero): void {
        this.displayResults = false;
        this.searchForm.nativeElement.reset();
        this.toggleSearch();
        this.dialog.open(SentinelsHeroVillainModalComponent, {
            width: '50rem',
            data: {hero: hero}
        });
    }

    private displaySearchResults(results): void {
        this.displayResults = true;
        this.noResults = results.length < 1;

        if (this.noResults) {
        }
    }
}
