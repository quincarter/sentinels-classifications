import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsSearchComponent } from './sentinels-search.component';

describe('SentinelsSearchComponent', () => {
  let component: SentinelsSearchComponent;
  let fixture: ComponentFixture<SentinelsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
