import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeroService } from '../../services/hero.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-sentinels-hero-list',
    templateUrl: './sentinels-hero-list.component.html',
    styleUrls: ['./sentinels-hero-list.component.scss']
})
export class SentinelsHeroListComponent implements OnInit, OnDestroy {

    public heroes: any;
    public filters = [
        '-All-',
        'Damage',
        'Tank',
        'Support',
        'Jack of all Trades',
        'Utility', 'Buffs',
        'Combo Builder',
        'Minions',
        'Irreducible Damage',
        'Outside of turn effects',
        'Burst',
        'AOE Damage',
        'Deck Manipulation',
        'Equipment based'
    ];
    public selectedFilter: string;

    private heroSubscription: Subscription;

    constructor(
        private heroService: HeroService
    ) {
    }

    ngOnInit() {
        this.selectedFilter = '-All-';
        this.filters.sort();
        this.heroService.getAllHeroes();
        this.getHeroSubscription();
        // this.getHeroesSearchSubscription();
    }

    ngOnDestroy(): void {
        this.heroSubscription.unsubscribe();
    }

    private getHeroSubscription(): void {
        this.heroSubscription = this.heroService.heroes$.subscribe(data => {
            this.heroes = data;
        });
    }
}
