import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsHeroListComponent } from './sentinels-hero-list.component';

describe('SentinelsHeroListComponent', () => {
  let component: SentinelsHeroListComponent;
  let fixture: ComponentFixture<SentinelsHeroListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsHeroListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsHeroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
