import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsVariantComponent } from './sentinels-variant.component';

describe('SentinelsVariantComponent', () => {
  let component: SentinelsVariantComponent;
  let fixture: ComponentFixture<SentinelsVariantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsVariantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsVariantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
