import { Component, Input, OnInit } from '@angular/core';
import { IVariant } from '../../models/Variants';

@Component({
  selector: 'app-sentinels-variant',
  templateUrl: './sentinels-variant.component.html',
  styleUrls: [ './sentinels-variant.component.scss']
})
export class SentinelsVariantComponent implements OnInit {

  constructor() { }

  @Input() variant: IVariant;

  ngOnInit() {
  }

}
