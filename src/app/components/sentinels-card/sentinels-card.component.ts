import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SentinelsHeroVillainModalComponent } from '../modals/sentinels-hero-villain-modal/sentinels-hero-villain-modal.component';
import { HeroService } from '../../services/hero.service';
import { Observable, Subscription } from 'rxjs';
import { Hero } from '../../models/heroes';


@Component({
    selector: 'app-sentinels-card',
    templateUrl: './sentinels-card.component.html',
    styleUrls: ['./sentinels-card.component.scss']
})
export class SentinelsCardComponent implements OnDestroy, OnInit {

    @Input() filter: string;
    @Input() hero: Hero;

    public firestoreHeroes: Observable<any[]>;
    public filteredHeroes: Observable<any[]>;

    private filteredSubscription: Subscription;
    private heroSubscription: Subscription;

    constructor(
        public dialog: MatDialog,
        private heroService: HeroService,
    ) {
    }

    ngOnInit() {
        this.getHeroSubscription();
        this.getFilteredSubscription();
    }

    ngOnDestroy() {
        this.heroSubscription.unsubscribe();
        this.filteredSubscription.unsubscribe();
    }

    openModal(hero: any): void {
        this.dialog.open(SentinelsHeroVillainModalComponent, {
            width: '50rem',
            data: {hero}
        });
    }

    checkForFilter(): any {
        const filter = this.filter.toLowerCase();
        if (filter === '-all-') {
            return this.firestoreHeroes;
        } else {
            return this.firestoreHeroes;
        }
    }

    private getHeroSubscription(): void {
        this.heroSubscription = this.heroService.heroes$.subscribe(heroes => {
            this.firestoreHeroes = heroes;
        });
    }

    private getFilteredSubscription(): void {
        this.filteredSubscription = this.heroService.filteredHeroes$.subscribe(heroes => {
            this.filteredHeroes = heroes;
        });
    }
}

/*if (primaryType && secondaryType) {
                    if (primaryType.toLowerCase().includes(this.filter.toLowerCase()) ||
                        secondaryType.toLowerCase().includes(this.filter.toLowerCase())) {
                        return true;
                    }
                } if (primaryType && !secondaryType) {
                    if (primaryType.toLowerCase().includes(this.filter.toLowerCase())) {
                        return true;
                    }
                } else {
                    return false;
                }*/
