import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentinelsCardComponent } from './sentinels-card.component';

describe('SentinelsCardComponent', () => {
  let component: SentinelsCardComponent;
  let fixture: ComponentFixture<SentinelsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentinelsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentinelsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
