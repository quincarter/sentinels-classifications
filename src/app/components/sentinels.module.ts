import { NgModule } from '@angular/core';
import { SentinelsTitleBarComponent } from './sentinels-title-bar/sentinels-title-bar.component';
import { AngularMaterialModulesModule } from '../materialImports/angularMaterialModules.module';
import { SentinelsCardComponent } from './sentinels-card/sentinels-card.component';
import { CommonModule } from '@angular/common';
import { SentinelsSearchComponent } from './sentinels-search/sentinels-search.component';
import { HeroService } from '../services/hero.service';
import { HttpClientModule } from '@angular/common/http';
import { SearchService } from '../services/search.service';
import { SentinelsHeroVillainModalComponent } from './modals/sentinels-hero-villain-modal/sentinels-hero-villain-modal.component';
import { SentinelsHeroListComponent } from './sentinels-hero-list/sentinels-hero-list.component';
import { SentinelsProfileComponent } from './sentinels-profile/sentinels-profile.component';
import { VillainslistComponent } from './villainslist/villainslist.component';
import { SentinelsVariantComponent } from './sentinels-variant/sentinels-variant.component';


@NgModule({
    declarations: [
        SentinelsTitleBarComponent,
        SentinelsCardComponent,
        SentinelsSearchComponent,
        SentinelsHeroVillainModalComponent,
        SentinelsHeroListComponent,
        SentinelsProfileComponent,
        VillainslistComponent,
        SentinelsVariantComponent,
    ],
    imports: [
        AngularMaterialModulesModule,
        CommonModule,
        HttpClientModule,
    ],
    exports: [
        SentinelsTitleBarComponent,
        SentinelsCardComponent,
        SentinelsSearchComponent,
        SentinelsHeroVillainModalComponent,
        SentinelsHeroListComponent,
        SentinelsProfileComponent,
        VillainslistComponent,
        SentinelsVariantComponent,
    ],
    providers: [
        HeroService,
        SearchService,
    ],
    entryComponents: [
        SentinelsHeroVillainModalComponent,
    ],
})
export class SentinelsModule {
}
