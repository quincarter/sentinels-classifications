import { Injectable } from '@angular/core';
import { Hero } from '../models/heroes';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Powers, Variant } from '../models/Variants';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class HeroService {

    private _heroes: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private _filteredHeroes: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private _variants: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    private heroArray: any[];

    public readonly filteredHeroes$: Observable<any> = this._filteredHeroes.asObservable();
    public readonly variants$: Observable<any> = this._variants.asObservable();
    public readonly heroes$: Observable<any> = this._heroes.asObservable();

    constructor(
        private http: HttpClient,
        private db: AngularFirestore
    ) {
        // this.getAllHeroes();
    }

    public getAllHeroes(): void {
        this._heroes.next(this.db.collection('/heroes').valueChanges()
            .pipe(
                tap((response: any) => {
                    response = response
                        .map(hero => new Hero(
                            hero.id,
                            hero.name,
                            hero.primary_class,
                            hero.secondary_class,
                            hero.image,
                            hero.box,
                            hero.complexity
                            )
                        );
                    this.heroArray = response;
                    return response;
                })
            ));
        this._filteredHeroes.next(this.db.collection('/heroes').valueChanges()
            .pipe(
                tap((response: any) => {
                    response = response
                        .map(hero => new Hero(
                            hero.id,
                            hero.name,
                            hero.primary_class,
                            hero.secondary_class,
                            hero.image,
                            hero.box,
                            hero.complexity
                            )
                        );
                    return response;
                })
            ));
    }

    getSearchList(): any {
        return this.heroArray;
    }

    getVariants(hero_id: string): void {
        this._variants.next(
            this.db.doc('/variants/' + hero_id).valueChanges()
                .pipe(
                    map((variants: any) => {
                        if (variants) {
                            variants = variants.variants
                                .map(variant => new Variant(
                                    variant.card_image,
                                    variant.hp,
                                    variant.name,
                                    variant.backstory,
                                    variant.powers
                                        .map(powers => new Powers(powers.name, powers.description))));
                            return variants;
                        } else {
                            return null;
                        }

                    })
                )
        );
    }

    search(filter: string): any {
        return this.heroArray
            .map(hero => new Hero(
                hero.id,
                hero.name,
                hero.primary_class,
                hero.secondary_class,
                hero.image,
                hero.box,
                hero.complexity
                )
            )
            // Not filtering in the server since in-memory-web-api has somewhat restricted api
            .filter(hero => hero.name.toLowerCase().includes(filter));
    }

    // todo filtering causing a memory leak right now -- need to revist
    /*filter(filter: string): void {
        console.log('_filteredHoeres', this.heroArray);

        this._filteredHeroes.next(this.db.collection('/heroes').stateChanges()
            .pipe(
                tap((response: any) => {
                    response = response
                        .map(hero => new Hero(hero.id, hero.name, hero.primary_class, hero.secondary_class, hero.image, hero.box, hero.complexity))
                        .filter(hero => {
                            if (hero.primary_class && hero.secondary_class) {
                                if (hero.primary_class.toLowerCase().includes(filter.toLowerCase()) ||
                                    hero.secondary_class.toLowerCase().includes(filter.toLowerCase())) {
                                    return true;
                                }
                            }
                            if (hero.primary_class && !hero.secondary_class) {
                                if (hero.primary_class.toLowerCase().includes(filter.toLowerCase())) {
                                    return true;
                                }
                            } else {
                                return false;
                            }
                        });
                    return response;
                })
            ));
    }*/
}
