import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';
import { SearchKeyNames, SearchKeys } from '../models/search';

@Injectable({
    providedIn: 'root'
})
export class SearchService {

    searchToggleSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {
    }

    toggleSearch(toggle: boolean): Observable<boolean> {
        this.searchToggleSubject.next(toggle);
        return this.searchToggleSubject.asObservable();
    }

    getSearchToggleStatus(): Observable<boolean> {
        return this.searchToggleSubject.asObservable();
    }

}

export function validateArrowKeys(keyCode: number) {
    return keyCode === SearchKeys.ArrowDown || keyCode === SearchKeys.ArrowUp;
}

export function isEnterKey(event: KeyboardEvent) {
    return event.keyCode === SearchKeys.Enter && event.key === SearchKeyNames.Enter;
}

export function isEscapeKey(event: KeyboardEvent) {
    return event.keyCode === SearchKeys.Escape && event.key === SearchKeyNames.Escape;
}

export function isIndexActive(index: number, currentIndex: number) {
    return index === currentIndex;
}

export function isUpArrowKey(event: KeyboardEvent) {
    return event.keyCode === SearchKeys.ArrowUp && event.key === SearchKeyNames.ArrowUp;
}

export function resolveNextIndex(
    currentIndex: number,
    stepUp: boolean,
    listLength = 10
) {
    const step = stepUp ? 1 : -1;
    const topLimit = listLength - 1;
    const bottomLimit = 0;
    const currentResultIndex = currentIndex + step;
    let resultIndex = currentResultIndex;
    if (currentResultIndex === topLimit + 1) {
        resultIndex = bottomLimit;
    }
    if (currentResultIndex === bottomLimit - 1) {
        resultIndex = topLimit;
    }
    return resultIndex;
}
