import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeroService } from '../../services/hero.service';
import { Subscription } from 'rxjs/index';
import { SearchService } from '../../services/search.service';
import { Hero } from '../../models/heroes';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit, OnDestroy {

    public heroList: Hero;
    public heroes: any;
    public heroesSearchSubscription: Subscription;
    public heroesSubscription: Subscription;
    public filters = [ '-All-', 'Damage', 'Tank', 'Support', 'Jack of all Trades', 'Utility', 'Buffs', 'Combo Builder', 'Minions', 'Irreducible Damage', 'Outside of turn effects', 'Burst', 'AOE Damage', 'Deck Manipulation', 'Equipment based' ];
    public selectedFilter: string;

    constructor(
        private heroService: HeroService,
        private searchService: SearchService
    ) {
    }

    ngOnInit() {
        this.selectedFilter = '-All-';
        this.filters.sort();
        // this.getHeroesSearchSubscription();
    }

    ngOnDestroy(): void {
    }
}
