import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SentinelsModule } from '../components/sentinels.module';
import { HeroService } from '../services/hero.service';
import { HttpClientModule } from '@angular/common/http';
import { SearchService } from '../services/search.service';
import { AngularMaterialModulesModule } from '../materialImports/angularMaterialModules.module';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

@NgModule({
    imports: [
        AngularMaterialModulesModule,
        CommonModule,
        SentinelsModule,
        HttpClientModule,
    ],
    declarations: [ HomeComponent, PageNotFoundComponent ],
    providers: [
        HeroService,
        SearchService,
    ],
    exports: [PageNotFoundComponent],
})
export class PagesModule {
}
