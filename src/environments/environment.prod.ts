export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyBkgnBkOeIIkdrtcyoccSuA0S4hBq_7CW8',
        authDomain: 'sentinels-classifications.firebaseapp.com',
        databaseURL: 'https://sentinels-classifications.firebaseio.com',
        projectId: 'sentinels-classifications',
        storageBucket: 'sentinels-classifications.appspot.com',
        messagingSenderId: '533142325838'
    }
};
