import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));


export const MAIN: any = {
    MQ_BREAKPOINTS: {
        MOBILE: 320,
        MOBILE_LARGE: 420,
        TABLET: 768,
        DESKTOP: 1024,
        DESKTOP_WIDE: 2000,
        DESKTOP_WIDER: 3000,
        DESKTOP_HUGE: 4000,
        TV: 5000,
        TV_WIDE: 6000,
        TV_WIDER: 7000,
        TV_HUGE: 8000,
    }
};